package Adrian.Góra.Music.application.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String address;
    private String district;
    private String city;
    private String postalCode;
    private String phone;

    @OneToOne(mappedBy = "address")
    private Artist artist;
}
