package Adrian.Góra.Music.application.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Album {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;
    private LocalDate releaseYear;

    @ManyToOne
    private Artist artist;

    @OneToMany
    @JoinColumn(name = "album_id")
    private List<Song> songList;
}
