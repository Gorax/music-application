package Adrian.Góra.Music.application.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Artist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;
    private String lastName;
    private String linkToImage;

    @OneToOne
    private Address address;

    @OneToMany
    @JoinColumn(name = "artist_id")
    private List<Album> albumList;
}
