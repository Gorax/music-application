package Adrian.Góra.Music.application.repository;

import Adrian.Góra.Music.application.model.Address;
import Adrian.Góra.Music.application.model.Artist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AddressRepository extends CrudRepository<Address, Integer> {

    Optional<Address> findByArtist(Artist artist);
}
