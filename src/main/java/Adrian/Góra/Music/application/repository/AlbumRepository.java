package Adrian.Góra.Music.application.repository;

import Adrian.Góra.Music.application.model.Album;
import Adrian.Góra.Music.application.model.Artist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AlbumRepository extends CrudRepository<Album, Integer> {

    List<Album> findAll();

    List<Album> findAllByArtist (Artist artist);

}
