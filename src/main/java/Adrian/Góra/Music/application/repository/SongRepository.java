package Adrian.Góra.Music.application.repository;

import Adrian.Góra.Music.application.model.Song;
import Adrian.Góra.Music.application.model.Album;
import Adrian.Góra.Music.application.model.Genre;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SongRepository extends CrudRepository<Song, Integer> {

    List<Song> findAll();

    List<Song> findAllByGenre(Genre genre);

    List<Song> findAllByAlbum(Album album);

}
