package Adrian.Góra.Music.application.service;

import Adrian.Góra.Music.application.model.Album;
import Adrian.Góra.Music.application.model.Artist;
import Adrian.Góra.Music.application.repository.AlbumRepository;
import Adrian.Góra.Music.application.repository.ArtistRepository;
import Adrian.Góra.Music.application.repository.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArtistStatisticService {

    @Autowired
    private ArtistRepository artistRepository;

    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private SongRepository songRepository;

    public int numberOfAllArtistAlbums(String artistIndex) {
        int numberOfAlbums = 0;

        if (artistRepository.findById(Integer.valueOf(artistIndex)).isPresent()) {
            Artist artist = artistRepository.findById(Integer.valueOf(artistIndex)).get();
            numberOfAlbums = albumRepository.findAllByArtist(artist).size();
        }

        return numberOfAlbums;
    }

    public double averageOfAllArtistAlbums(String artistIndex) {
        int numberOfAllAlbums = albumRepository.findAll().size();
        int numberOfArtistAlbums = numberOfAllArtistAlbums(artistIndex);
        double average;

        average = numberOfArtistAlbums * 100 / numberOfAllAlbums;

        return average;
    }

    public int numberOfAllArtistSongs(String artistIndex) {
        int numberOfSongs = 0;

        if (artistRepository.findById(Integer.valueOf(artistIndex)).isPresent()) {
            Artist artist = artistRepository.findById(Integer.valueOf(artistIndex)).get();
            List<Album> albumList = albumRepository.findAllByArtist(artist);

            for (int i = 0; i < albumList.size(); i++) {
                numberOfSongs += songRepository.findAllByAlbum(albumList.get(i)).size();
            }
        }

        return numberOfSongs;
    }

    public double averageOfAllArtistSongs(String artistIndex) {
        int numberOfAllSongs = songRepository.findAll().size();
        int numberOfArtistSongs = numberOfAllArtistSongs(artistIndex);
        double average;

        average = numberOfArtistSongs * 100 / numberOfAllSongs;

        return average;
    }
}
