package Adrian.Góra.Music.application.service;

import Adrian.Góra.Music.application.model.*;
import Adrian.Góra.Music.application.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class InitDataLoader {

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private ArtistRepository artistRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private SongRepository songRepository;

    @PostConstruct
    public void init() {
        genreRepository.save(new Genre(1, "Pop"));
        genreRepository.save(new Genre(2, "Pop / Punk"));
        genreRepository.save(new Genre(3, "Rock"));
        genreRepository.save(new Genre(4, "Pop / Rock"));
        genreRepository.save(new Genre(5, "Punk"));
        genreRepository.save(new Genre(6, "Dance / Electronic / House"));
        genreRepository.save(new Genre(7, "Hip-Hop / Rap / Trap"));
        genreRepository.save(new Genre(8, "Jazz"));
        genreRepository.save(new Genre(9, "Classical / Opera"));
        genreRepository.save(new Genre(10, "Metal"));

        List<Song> album1SongList = new ArrayList<>();
        album1SongList.add(songRepository.save(new Song(1, "Losing Grip", 3.53, "English", null, genreRepository.findById(4).get())));
        album1SongList.add(songRepository.save(new Song(2, "Complicated", 4.04, "English", null, genreRepository.findById(4).get())));

        List<Song> album2SongList = new ArrayList<>();
        album2SongList.add(songRepository.save(new Song(3, "Take Me Away", 2.57, "English", null, genreRepository.findById(4).get())));
        album2SongList.add(songRepository.save(new Song(4, "My Happy Ending", 4.02, "English", null, genreRepository.findById(4).get())));

        List<Song> album3SongList = new ArrayList<>();
        album3SongList.add(songRepository.save(new Song(5, "Girlfriend", 3.36, "English", null, genreRepository.findById(4).get())));
        album3SongList.add(songRepository.save(new Song(6, "The Best Damn Thing", 3.09, "English", null, genreRepository.findById(4).get())));

        List<Album> albumList = new ArrayList<>();
        albumList.add(albumRepository.save(new Album(1, "Let Go", LocalDate.parse("2002-06-04"), null, album1SongList)));
        albumList.add(albumRepository.save(new Album(2, "Under My Skin", LocalDate.parse("2004-05-25"), null, album2SongList)));
        albumList.add(albumRepository.save(new Album(3, "The Best Damn Thing", LocalDate.parse("2007-02-26"), null, album3SongList)));

        addressRepository.save(new Address(1, "Suite 100", "Civic Center Drive", "Beverly Hills", "90210-3629", "132-211-211", null));

        artistRepository.save(new Artist(1, "Avrill", "Lavinge", "https://gogomagazine.it/wp-content/uploads/2018/01/avril-lavigne.jpg", addressRepository.findById(1).get(), albumList));
    }
}
