package Adrian.Góra.Music.application.view;

import Adrian.Góra.Music.application.model.Song;
import Adrian.Góra.Music.application.repository.*;
import Adrian.Góra.Music.application.model.Address;
import Adrian.Góra.Music.application.model.Album;
import Adrian.Góra.Music.application.model.Artist;
import Adrian.Góra.Music.application.model.Genre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Controller
@Validated
public class AddNew {

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private ArtistRepository artistRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private SongRepository songRepository;

    @GetMapping("addNew")
    public ModelAndView basicView(Model model) {
        List<Genre> genreList = genreRepository.findAll();
        model.addAttribute("genreList", genreList);
        return new ModelAndView("addNew");
    }

    @PostMapping("newArtist")
    public String addArtist(@Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String firstName, @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String lastName,
                            String linkToImage, @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String address,
                            @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String district, @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String city,
                            @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String postalCode, @Validated @NotBlank @Size(min = 11, max = 11) @RequestParam String phone) {

        Address newAddress = new Address(null, address, district, city, postalCode, phone, null);
        addressRepository.save(newAddress);
        Artist newArtist = new Artist(null, firstName, lastName, linkToImage, newAddress, new ArrayList<>());
        artistRepository.save(newArtist);

        return "addNew";
    }

    @PostMapping("newAlbum")
    public String newAlbum(@Validated @NotBlank @Size(min = 3, max = 50) @RequestParam String albumName, @Validated @NotNull @RequestParam String releaseYear) {
        Album album = new Album(null, albumName, LocalDate.parse(releaseYear), null, new ArrayList<>());
        albumRepository.save(album);
        return "addNew";
    }

    @PostMapping("newSong")
    public String newSong(@Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String songTitle, @Validated @NotNull @DecimalMin("0.00") @DecimalMax("9.99") @RequestParam double length,
                          @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String language, @Validated @NotBlank @RequestParam String genreId) {

        Genre genre = genreRepository.findById(Integer.valueOf(genreId)).get();
        Song song = new Song(null, songTitle, length, language, null, genre);
        songRepository.save(song);

        return "addNew";
    }

    @PostMapping("newGenre")
    public String newGenre(@Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String genre) {

        Genre newGenre = new Genre(null, genre);
        genreRepository.save(newGenre);
        return "addNew";
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public ModelAndView conflict() {
        return new ModelAndView("addNew");
    }
}
