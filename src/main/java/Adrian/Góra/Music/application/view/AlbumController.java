package Adrian.Góra.Music.application.view;

import Adrian.Góra.Music.application.repository.AlbumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AlbumController {

    @Autowired
    private AlbumRepository albumRepository;

    @GetMapping("album")
    public ModelAndView allAlbums (Model model){
        model.addAttribute("albums", albumRepository.findAll());
        return new ModelAndView("albums");
    }
}
