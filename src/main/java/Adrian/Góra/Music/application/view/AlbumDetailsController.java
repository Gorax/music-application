package Adrian.Góra.Music.application.view;

import Adrian.Góra.Music.application.model.Song;
import Adrian.Góra.Music.application.repository.AlbumRepository;
import Adrian.Góra.Music.application.repository.SongRepository;
import Adrian.Góra.Music.application.model.Album;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Controller()
@Validated
public class AlbumDetailsController {

    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private SongRepository songRepository;

    @GetMapping("albumDetails")
    public ModelAndView albumDetails(@Validated @NotBlank @RequestParam String index, Model model) {
        basicView(index, model);
        return new ModelAndView("albumDetails");
    }

    @GetMapping("albumDetailsEdit")
    public ModelAndView albumDetailsEdit(@Validated @NotBlank @RequestParam String index, Model model) {
        List<Song> freeSongs = songRepository.findAllByAlbum(null);

        basicView(index, model);
        model.addAttribute("freeSongs", freeSongs);
        return new ModelAndView("albumDetailsEdit");
    }

    private void basicView(String index, Model model) {
        Album album = null;
        List<Song> songList = null;

        if (albumRepository.findById(Integer.valueOf(index)).isPresent()) {
            album = albumRepository.findById(Integer.valueOf(index)).get();
            songList = songRepository.findAllByAlbum(album);
        }

        model.addAttribute("album", album);
        model.addAttribute("songList", songList);
    }

    @PostMapping("albumDetailsEditAlbumUpdate")
    public String albumUpdate(@Validated @NotBlank @Size(min = 3, max = 50) @RequestParam String albumName, @Validated @NotNull @RequestParam String releaseYear,
                              @Validated @NotBlank @RequestParam String index) {

        if (albumRepository.findById(Integer.valueOf(index)).isPresent()) {
            Album album = albumRepository.findById(Integer.valueOf(index)).get();
            album.setName(albumName);
            album.setReleaseYear(LocalDate.parse(releaseYear));
            albumRepository.save(album);
        }
        return "redirect:albumDetailsEdit?index=" + index;
    }

    @PostMapping("albumDetailsEditAlbumDelete")
    public String albumDelete(@Validated @NotBlank @RequestParam String index) {

        if (albumRepository.findById(Integer.valueOf(index)).isPresent()) {
            Album album = albumRepository.findById(Integer.valueOf(index)).get();
            albumRepository.delete(album);
        }

        return "redirect:album";
    }

    @PostMapping("albumDetailsEditSongAdd")
    public String songAdd(@Validated @NotBlank @RequestParam String songIndex, @Validated @NotBlank @RequestParam String albumIndex) {

        if (albumRepository.findById(Integer.valueOf(albumIndex)).isPresent()) {
            Album album = albumRepository.findById(Integer.valueOf(albumIndex)).get();
            if (songRepository.findById(Integer.valueOf(songIndex)).isPresent()) {
                Song song = songRepository.findById(Integer.valueOf(songIndex)).get();
                album.getSongList().add(song);
                albumRepository.save(album);
            }
        }
        return "redirect:albumDetailsEdit?index=" + albumIndex;
    }

    @PostMapping("albumDetailsEditSongRemove")
    public String songRemove(@Validated @NotBlank @RequestParam String songIndex, @Validated @NotBlank @RequestParam String albumIndex) {

        if (albumRepository.findById(Integer.valueOf(albumIndex)).isPresent()) {
            Album album = albumRepository.findById(Integer.valueOf(albumIndex)).get();
            if (songRepository.findById(Integer.valueOf(songIndex)).isPresent()) {
                Song song = songRepository.findById(Integer.valueOf(songIndex)).get();
                album.getSongList().remove(song);
                albumRepository.save(album);
            }
        }
        return "redirect:albumDetailsEdit?index=" + albumIndex;
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public ModelAndView conflict() {
        return new ModelAndView("album");
    }
}

