package Adrian.Góra.Music.application.view;

import Adrian.Góra.Music.application.repository.ArtistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ArtistController {

    @Autowired
    private ArtistRepository artistRepository;

    @GetMapping("artist")
    public ModelAndView allArtists(Model model) {
        model.addAttribute("artists", artistRepository.findAll());
        return new ModelAndView("/artists");
    }
}
