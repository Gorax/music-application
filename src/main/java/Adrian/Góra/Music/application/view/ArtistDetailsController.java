package Adrian.Góra.Music.application.view;

import Adrian.Góra.Music.application.repository.AlbumRepository;
import Adrian.Góra.Music.application.repository.ArtistRepository;
import Adrian.Góra.Music.application.service.ArtistStatisticService;
import Adrian.Góra.Music.application.model.Address;
import Adrian.Góra.Music.application.model.Album;
import Adrian.Góra.Music.application.model.Artist;
import Adrian.Góra.Music.application.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Controller
@Validated
public class ArtistDetailsController {

    @Autowired
    private ArtistRepository artistRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private ArtistStatisticService artistStatisticService;

    @GetMapping("artistDetails")
    public ModelAndView artistDetails(@Validated @NotBlank @RequestParam String index, Model model) {
        basicView(index, model);
        setStatistic(index, model);
        return new ModelAndView("artistDetails");
    }

    @GetMapping("artistDetailsEdit")
    public ModelAndView artistDetailsEdit(@Validated @NotBlank @RequestParam String index, Model model) {
        List<Album> freeAlbums = albumRepository.findAllByArtist(null);

        basicView(index, model);
        model.addAttribute("freeAlbums", freeAlbums);
        return new ModelAndView("artistDetailsEdit");
    }

    private void basicView(@Validated @NotBlank @RequestParam String index, Model model) {
        Artist artist = null;
        Address address = null;
        List<Album> albums = null;

        if (artistRepository.findById(Integer.valueOf(index)).isPresent()) {
            artist = artistRepository.findById(Integer.valueOf(index)).get();
            albums = albumRepository.findAllByArtist(artist);
        }

        if (addressRepository.findByArtist(artist).isPresent()) {
            address = addressRepository.findByArtist(artist).get();
        }

        model.addAttribute("artist", artist);
        model.addAttribute("address", address);
        model.addAttribute("albums", albums);
    }

    private void setStatistic(String index, Model model) {
        model.addAttribute("numberOfAllAlbums", artistStatisticService.numberOfAllArtistAlbums(index));
        model.addAttribute("averageOfAllAlbums", artistStatisticService.averageOfAllArtistAlbums(index));
        model.addAttribute("numberOfAllSongs", artistStatisticService.numberOfAllArtistSongs(index));
        model.addAttribute("averageOfAllSongs", artistStatisticService.averageOfAllArtistSongs(index));
    }

    @PostMapping("artistDetailsEditArtistUpdate")
    public String artistUpdate(@Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String firstName, @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String lastName,
                               @RequestParam String linkToImage, @Validated @NotBlank @RequestParam String index) {

        if (artistRepository.findById(Integer.valueOf(index)).isPresent()) {
            Artist artist = artistRepository.findById(Integer.valueOf(index)).get();
            artist.setFirstName(firstName);
            artist.setLastName(lastName);
            artist.setLinkToImage(linkToImage);
            artistRepository.save(artist);
        }

        return "redirect:artistDetails?index=" + index;
    }

    @PostMapping("artistDetailsEditAddressUpdate")
    public String addressUpdate(@Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String address, @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String district,
                                @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String city, @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String postalCode,
                                @Validated @NotBlank @Size(min = 11, max = 11) @RequestParam String phone, @Validated @NotBlank @RequestParam String index) {

        if (artistRepository.findById(Integer.valueOf(index)).isPresent()) {
            Artist artist = artistRepository.findById(Integer.valueOf(index)).get();
            if (addressRepository.findByArtist(artist).isPresent()) {
                Address editAddress = addressRepository.findByArtist(artist).get();
                editAddress.setAddress(address);
                editAddress.setDistrict(district);
                editAddress.setCity(city);
                editAddress.setPostalCode(postalCode);
                editAddress.setPhone(phone);
                addressRepository.save(editAddress);
            }
        }

        return "redirect:artistDetails?index=" + index;
    }

    @PostMapping("artistDetailsEditArtistDelete")
    public String artistDelete(@Validated @NotBlank @RequestParam String index) {

        if (artistRepository.findById(Integer.valueOf(index)).isPresent()) {
            Artist artist = artistRepository.findById(Integer.valueOf(index)).get();
            if (addressRepository.findByArtist(artist).isPresent()) {
                Address address = addressRepository.findByArtist(artist).get();
                addressRepository.delete(address);
            }
            artistRepository.delete(artist);
        }

        return "redirect:artist";
    }

    @PostMapping("artistDetailsEditAddAlbum")
    public String albumAdd(@Validated @NotBlank @RequestParam String artistIndex, @Validated @NotBlank @RequestParam String albumIndex) {

        if (artistRepository.findById(Integer.valueOf(artistIndex)).isPresent()) {
            Artist artist = artistRepository.findById(Integer.valueOf(artistIndex)).get();
            if (albumRepository.findById(Integer.valueOf(albumIndex)).isPresent()) {
                Album album = albumRepository.findById(Integer.valueOf(albumIndex)).get();
                artist.getAlbumList().add(album);
                artistRepository.save(artist);
            }
        }

        return "redirect:artistDetailsEdit?index=" + artistIndex;
    }

    @PostMapping("artistDetailsEditAlbumRemove")
    public String albumRemove(@Validated @NotBlank @RequestParam String artistIndex, @Validated @NotBlank @RequestParam String albumIndex) {

        if (albumRepository.findById(Integer.valueOf(albumIndex)).isPresent()) {
            Album album = albumRepository.findById(Integer.valueOf(albumIndex)).get();
            album.setArtist(null);
            albumRepository.save(album);
        }

        return "redirect:artistDetailsEdit?index=" + artistIndex;
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public ModelAndView conflict() {
        return new ModelAndView("artist");
    }
}
