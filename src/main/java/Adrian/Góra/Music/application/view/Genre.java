package Adrian.Góra.Music.application.view;

import Adrian.Góra.Music.application.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Genre {

    @Autowired
    private GenreRepository genreRepository;

    @GetMapping("genre")
    public ModelAndView allGenres(Model model){
        model.addAttribute("genres", genreRepository.findAll());
        return new ModelAndView("genres");
    }
}
