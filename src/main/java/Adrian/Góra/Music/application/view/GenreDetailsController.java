package Adrian.Góra.Music.application.view;

import Adrian.Góra.Music.application.model.Song;
import Adrian.Góra.Music.application.repository.SongRepository;
import Adrian.Góra.Music.application.model.Genre;
import Adrian.Góra.Music.application.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Controller
@Validated
public class GenreDetailsController {

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private SongRepository songRepository;

    @GetMapping("genreDetailsEdit")
    public ModelAndView genreDetails(@Validated @NotBlank @RequestParam String index, Model model) {
        Genre genre = null;

        if (genreRepository.findById(Integer.valueOf(index)).isPresent()) {
            genre = genreRepository.findById(Integer.valueOf(index)).get();
        }

        model.addAttribute("genre", genre);
        return new ModelAndView("genreDetailsEdit");
    }

    @PostMapping("genreDetailsEditGenreUpdate")
    public String genreUpdate(@Validated @NotBlank @RequestParam String index, @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String type) {

        if (genreRepository.findById(Integer.valueOf(index)).isPresent()) {
            Genre genre = genreRepository.findById(Integer.valueOf(index)).get();
            genre.setName(type);
            genreRepository.save(genre);
        }
        return "redirect:genreDetailsEdit?index=" + index;
    }

    @PostMapping("genreDetailsEditGenreDelete")
    public String genreDelete(@Validated @NotBlank @RequestParam String index) {

        if (genreRepository.findById(Integer.valueOf(index)).isPresent()) {
            Genre genre = genreRepository.findById(Integer.valueOf(index)).get();
            List<Song> songList = songRepository.findAllByGenre(genre);

            for (int i = 0; i < songList.size(); i++) {
                Song song = songList.get(i);
                song.setGenre(null);
            }
            genreRepository.delete(genre);
        }

        return "redirect:genre";
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public ModelAndView conflict() {
        return new ModelAndView("genre");
    }
}
