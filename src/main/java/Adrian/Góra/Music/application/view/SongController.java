package Adrian.Góra.Music.application.view;

import Adrian.Góra.Music.application.repository.SongRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SongController {

    @Autowired
    private SongRepository songRepository;

    @GetMapping("song")
    public ModelAndView allSongs(Model model){
        model.addAttribute("songs", songRepository.findAll());
        return new ModelAndView("songs");
    }
}
