package Adrian.Góra.Music.application.view;

import Adrian.Góra.Music.application.model.Song;
import Adrian.Góra.Music.application.repository.SongRepository;
import Adrian.Góra.Music.application.model.Genre;
import Adrian.Góra.Music.application.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.*;
import java.util.List;

@Controller
@Validated
public class SongDetailsController {

    @Autowired
    private SongRepository songRepository;

    @Autowired
    private GenreRepository genreRepository;

    @GetMapping("songDetails")
    public ModelAndView songDetails(@Validated @NotBlank @RequestParam String index, Model model) {
        basicView(index, model);
        return new ModelAndView("songDetails");
    }

    @GetMapping("songDetailsEdit")
    public ModelAndView songDetailsEdit(@Validated @NotBlank @RequestParam String index, Model model) {
        List<Genre> genreList = genreRepository.findAll();

        basicView(index, model);
        model.addAttribute("genreList", genreList);
        return new ModelAndView("songDetailsEdit");
    }

    private void basicView(String index, Model model) {

        Song song = null;
        Genre genre = null;

        if (songRepository.findById(Integer.valueOf(index)).isPresent()) {
            song = songRepository.findById(Integer.valueOf(index)).get();

            if (song.getGenre() != null) {
                genre = genreRepository.findById(song.getGenre().getId()).get();
            }
        }

        model.addAttribute("song", song);
        model.addAttribute("genre", genre);
    }

    @PostMapping("songDetailsEditSongUpdate")
    public String songUpdate(@Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String songTitle, @Validated @NotNull @DecimalMin("0.00") @DecimalMax("9.99") @RequestParam double length,
                             @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String language, @Validated @NotBlank @RequestParam String index) {

        if (songRepository.findById(Integer.valueOf(index)).isPresent()) {
            Song song = songRepository.findById(Integer.valueOf(index)).get();
            song.setTitle(songTitle);
            song.setLength(length);
            song.setLanguage(language);
            songRepository.save(song);
        }

        return "redirect:songDetailsEdit?index=" + index;
    }

    @PostMapping("songDetailsEditSongDelete")
    public String songDelete(@Validated @NotBlank @RequestParam String index) {

        if (songRepository.findById(Integer.valueOf(index)).isPresent()) {
            Song song = songRepository.findById(Integer.valueOf(index)).get();
            songRepository.delete(song);
        }

        return "redirect:song";
    }

    @PostMapping("songDetailsEditGenreUpdate")
    public String genreChange(@Validated @NotBlank @RequestParam String songIndex, @Validated @NotBlank @RequestParam String genreIndex) {

        if (songRepository.findById(Integer.valueOf(songIndex)).isPresent()) {
            Song song = songRepository.findById(Integer.valueOf(songIndex)).get();
            if (genreRepository.findById(Integer.valueOf(genreIndex)).isPresent()) {
                Genre genre = genreRepository.findById(Integer.valueOf(genreIndex)).get();
                song.setGenre(genre);
                songRepository.save(song);
            }
        }

        return "redirect:songDetailsEdit?index=" + songIndex;
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public ModelAndView conflict() {
        return new ModelAndView("songDetails");
    }
}
